FROM openjdk:8-jdk-alpine AS builder
ARG CI_COMMIT_REF_NAME

LABEL stage=builder
WORKDIR /app
ADD ./ /app/
RUN cd /app
RUN ./mvnw versions:set -DnewVersion=$CI_COMMIT_REF_NAME
RUN ./mvnw clean install spring-boot:repackage

FROM openjdk:8-jdk-alpine
WORKDIR /app
COPY --from=builder /app/target/cdaas-demo-app-*.jar app.jar
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "/app/app.jar" ]